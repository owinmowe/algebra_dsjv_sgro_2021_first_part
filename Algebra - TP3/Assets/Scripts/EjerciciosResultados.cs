﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathDebbuger;

public class EjerciciosResultados : MonoBehaviour
{
    enum Ejercicio { Uno, Dos, Tres, Cuatro, Cinco, Seis, Siete, Ocho, Nueve, Diez };
    [SerializeField] Ejercicio ejercicio = default;
    [SerializeField] Color color = Color.red;
    [SerializeField] Vec3 A = Vec3.Zero;
    [SerializeField] Vec3 B = Vec3.Zero;
    Vector3 C;
    float aux = 0;

    private void Start()
    {
        Vector3Debugger.AddVector(A, Color.white, "A");
        Vector3Debugger.AddVector(B, Color.black, "B");
        Vector3Debugger.AddVector(C, color, "C");
    }

    private void Update()
    {
        switch (ejercicio)
        {
            case Ejercicio.Uno: //Suma de A y B
                C = A + B;
                break;
            case Ejercicio.Dos: //Resta de B menos A
                C = B - A;
                break;
            case Ejercicio.Tres: //Multiplicacion de A y B
                C.x = A.x * B.x;
                C.y = A.y * B.y;
                C.z = A.z * B.z;
                break;
            case Ejercicio.Cuatro: //Producto cruz entre A y B
                C = Vec3.Cross(B, A);
                break;
            case Ejercicio.Cinco: //Lerp entre A y B con limite en 1
                aux += Time.deltaTime;
                C = Vec3.Lerp(B, A, aux);
                if (aux > 1) { aux = 0; }
                break;
            case Ejercicio.Seis: //Vector resultando de los componentes maximos entre A y B
                C = Vec3.Max(A, B);
                break;
            case Ejercicio.Siete: //Proyeccion de A sobre B
                C = Vec3.Project(A, B.normalized);
                break;
            case Ejercicio.Ocho: //Tangente del vector formado entre A y B
                C = (A + B).normalized * Vec3.Distance(A, B);
                break;
            case Ejercicio.Nueve: //Reflecion de A sobre B
                C = Vec3.Reflect(A, B.normalized);
                break;
            case Ejercicio.Diez: //Lerp unclamped entre A y B hasta el numero 10
                aux += Time.deltaTime;
                C = Vec3.LerpUnclamped(A, B, aux);
                if (aux > 10) { aux = 0; }
                break;
        }
        Vector3Debugger.UpdatePosition("A", A);
        Vector3Debugger.UpdatePosition("B", B);
        Vector3Debugger.UpdatePosition("C", C);
        Vector3Debugger.UpdateColor("C", color);
    }

}
