﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeInsideTest : MonoBehaviour
{
    [SerializeField] GameObject[] walls = null;
    [SerializeField] GameObject cube = null;

    NewPlane[] planes;

    private void Start()
    {
        planes = new NewPlane[walls.Length];
        planes[0].SetNormalAndPosition(transform.right, walls[0].transform.position);
        planes[1].SetNormalAndPosition(transform.up, walls[1].transform.position);
        planes[2].SetNormalAndPosition(-transform.right, walls[2].transform.position);
        planes[3].SetNormalAndPosition(-transform.up, walls[3].transform.position);
        planes[4].SetNormalAndPosition(-transform.forward, walls[4].transform.position);
        planes[5].SetNormalAndPosition(transform.forward, walls[5].transform.position);
    }

    void Update()
    {
        int planesSameSide = 0;
        for (int i = 0; i < planes.Length; i++)
        {
            if(CheckGeometry(cube, planes[i]))
            {
                planesSameSide++;
            }
        }
        if (planesSameSide == planes.Length)
        {
            cube.GetComponent<Renderer>().enabled = true;
        }
        else
        {
            cube.GetComponent<Renderer>().enabled = false;
        }
    }

    bool CheckGeometry(GameObject go, NewPlane plane)
    {
        Vector3[] vertices = go.GetComponent<MeshFilter>().mesh.vertices;
        foreach (Vector3 v in vertices)
        {
            Vector3 world_v = go.transform.TransformPoint(v);
            if (plane.SameSide(plane.normal, world_v))
            {
                return true;
            }
        }
        return false;
    }

}
