﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public struct NewPlane
{
    public Vector3 normal { get; set; }

    public float distance { get; set; }

    public NewPlane(Vector3 inNormal, Vector3 inPoint)
    {
        normal = Vector3.Normalize(inNormal);
        distance = -Vector3.Dot(normal, inPoint);
    }

    public NewPlane(Vector3 inNormal, float d)
    {
        normal = Vector3.Normalize(inNormal);
        distance = d;
    }

    public NewPlane(Vector3 a, Vector3 b, Vector3 c)
    {
        normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));
        distance = -Vector3.Dot(normal, a);
    }

    public void SetNormalAndPosition(Vector3 inNormal, Vector3 inPoint)
    {
        normal = Vector3.Normalize(inNormal);
        distance = -Vector3.Dot(inNormal, inPoint);
    }

    public void Set3Points(Vector3 a, Vector3 b, Vector3 c)
    {
        normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));
        distance = -Vector3.Dot(normal, a);
    }

    public void Flip()
    {
        normal = -normal;
        distance = -distance;
    }

    public NewPlane flipped()
    {
        return new NewPlane(-normal, -distance);
    }

    public void Translate(Vector3 translation)
    {
        distance += Vector3.Dot(normal, translation);
    }

    public static NewPlane Translate(NewPlane plane, Vector3 translation)
    {
        return new NewPlane(plane.normal, plane.distance += Vector3.Dot(plane.normal, translation));
    }

    public Vector3 ClosestPointOnPlane(Vector3 point)
    {
        return point - normal * GetDistanceToPoint(point);
    }

    public float GetDistanceToPoint(Vector3 point)
    {
        return Vector3.Dot(normal, point) + distance;
    }

    public bool GetSide(Vector3 point)
    {
        return GetDistanceToPoint(point) > -Mathf.Epsilon;
    }

    public bool SameSide(Vector3 inPt0, Vector3 inPt1)
    {
        float distanceToPoint1 = GetDistanceToPoint(inPt0);
        float distanceToPoint2 = GetDistanceToPoint(inPt1);
        return distanceToPoint1 > 0.0 && distanceToPoint2 > 0.0 || distanceToPoint1 <= 0.0 && distanceToPoint2 <= 0.0;
    }
    /*
    public bool Raycast(Ray ray, out float enter)
    {
        float a = Vector3.Dot(ray.direction, normal);
        float num = -Vector3.Dot(ray.origin, normal) - distance;
        if (a < Mathf.Epsilon)
        {
            enter = 0.0f;
            return false;
        }
        enter = num / a;
        return enter > 0.0;
    }
    */
}
