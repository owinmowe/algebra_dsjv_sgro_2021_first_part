﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFrustrumCulling : MonoBehaviour
{

    [Header("Objects Related")]
    [SerializeField] int objectsLayer = 9;
    List<Renderer> objectsToRender = new List<Renderer>();
    NewPlane[] planes = new NewPlane[6];
    Vector3 nearTopLeft, nearTopRight, nearBottomLeft, nearBottomRight, farTopLeft, farTopRight, farBottomLeft, farBottomRight;
    Camera cam;

    [Header("Debug Related")]
    [SerializeField] bool debbuging = false;
    [SerializeField] bool autoRotate = false;
    [SerializeField] float rotationSpeed = 10;
    List<GameObject> listCubos = new List<GameObject>();

    private void Awake()
    {
        cam = GetComponent<Camera>();
    }

    // Start is called before the first frame update
    void Start()
    {
        FillListOfObjectsToRender();
        FillPlanesArray();
        DebugStart();
    }

    private void FillListOfObjectsToRender()
    {
        var goArray = FindObjectsOfType<GameObject>();
        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == objectsLayer)
                objectsToRender.Add(goArray[i].GetComponent<Renderer>());
        }
    }

    private void FillPlanesArray()
    {
        for (int i = 0; i < 6; i++)
        {
            planes[i] = new NewPlane();
        }
    }

    // Update is called once per frame
    void Update()
    {
        SetPlanesNewPoints();
        SetPlanesNormalToCenter();
        RenderObjectsOnFrustum();
        DebugUpdate();
    }

    private void RenderObjectsOnFrustum()
    {
        foreach (var rend in objectsToRender)
        {
            int planesSameSide = 0;
            for (int i = 0; i < planes.Length; i++)
            {
                if (CheckGeometry(rend.gameObject, planes[i]))
                {
                    planesSameSide++;
                }
            }
            if (planesSameSide == planes.Length)
            {
                rend.enabled = true;
            }
            else
            {
                rend.enabled = false;
            }
        }
    }

    private void SetPlanesNewPoints()
    {
        Vector3 farClipCenterPosition = transform.position + transform.forward * cam.farClipPlane;
        Vector3 nearClipCenterPosition = transform.position + transform.forward * cam.nearClipPlane;

        float HeightNear = (Mathf.Tan(Mathf.Deg2Rad * (cam.fieldOfView / 2)) * cam.nearClipPlane);
        float WidthNear = HeightNear * cam.aspect;

        float HeightFar = (Mathf.Tan(Mathf.Deg2Rad * (cam.fieldOfView / 2)) * cam.farClipPlane);
        float WidthFar = HeightFar * cam.aspect;

        nearTopLeft = nearClipCenterPosition + (transform.up * HeightNear) - (transform.right * WidthNear);
        nearTopRight = nearClipCenterPosition + (transform.up * HeightNear) + (transform.right * WidthNear);
        nearBottomLeft = nearClipCenterPosition - (transform.up * HeightNear) - (transform.right * WidthNear);
        nearBottomRight = nearClipCenterPosition - (transform.up * HeightNear) + (transform.right * WidthNear);
        farTopLeft = farClipCenterPosition + (transform.up * HeightFar) - (transform.right * WidthFar);
        farTopRight = farClipCenterPosition + (transform.up * HeightFar) + (transform.right * WidthFar);
        farBottomLeft = farClipCenterPosition - (transform.up * HeightFar) - (transform.right * WidthFar);
        farBottomRight = farClipCenterPosition - (transform.up * HeightFar) + (transform.right * WidthFar);

        planes[0].Set3Points(farTopRight, farTopLeft, farBottomLeft); // far
        planes[1].Set3Points(nearTopLeft, nearTopRight, nearBottomLeft); // Near
        planes[2].Set3Points(transform.position, farBottomLeft, farTopLeft); //Left
        planes[3].Set3Points(transform.position, farBottomRight, farBottomLeft); //Down
        planes[4].Set3Points(transform.position, farTopRight, farBottomRight); //Right
        planes[5].Set3Points(transform.position, farTopLeft, farTopRight); //Up
    }

    bool CheckGeometry(GameObject go, NewPlane plane)
    {
        Vector3[] vertices = go.GetComponent<MeshFilter>().mesh.vertices;
        foreach (Vector3 v in vertices)
        {
            Vector3 world_v = go.transform.TransformPoint(v);
            if (plane.SameSide(plane.normal, world_v))
            {
                return true;
            }
        }
        return false;
    }

    void SetPlanesNormalToCenter()
    {
        Vector3 frustumCenterPosition = transform.position + transform.forward * ((cam.farClipPlane + cam.nearClipPlane) / 2);
        for (int i = 0; i < planes.Length; i++)
        {
            if(!planes[i].SameSide(planes[i].normal, frustumCenterPosition))
            {
                planes[i].Flip();
            }
        }
        planes[0].normal = ((transform.position + transform.forward * cam.farClipPlane) - frustumCenterPosition).normalized;
        planes[1].normal = ((transform.position + transform.forward * cam.nearClipPlane) - frustumCenterPosition).normalized;
    }

    void DebugStart()
    {
        for (int i = 0; i < 9; i++)
        {
            listCubos.Add(GameObject.CreatePrimitive(PrimitiveType.Cube));
            listCubos[i].transform.parent = transform;
        }
        listCubos[0].name = "ntl";
        listCubos[1].name = "ntr";
        listCubos[2].name = "nbl";
        listCubos[3].name = "nbr";
        listCubos[4].name = "ftl";
        listCubos[5].name = "ftr";
        listCubos[6].name = "fbl";
        listCubos[7].name = "fbr";
        listCubos[8].name = "center";
    }

    void DebugUpdate()
    {
        if (debbuging)
        {
            listCubos[0].transform.position = nearTopLeft;
            listCubos[1].transform.position = nearTopRight;
            listCubos[2].transform.position = nearBottomLeft;
            listCubos[3].transform.position = nearBottomRight;
            listCubos[4].transform.position = farTopLeft;
            listCubos[5].transform.position = farTopRight;
            listCubos[6].transform.position = farBottomLeft;
            listCubos[7].transform.position = farBottomRight;
            listCubos[8].transform.position = transform.position + transform.forward * ((cam.farClipPlane + cam.nearClipPlane) / 2);
            for (int i = 0; i < listCubos.Count; i++)
            {
                listCubos[i].GetComponent<MeshRenderer>().enabled = true;
            }
        }
        else
        {
            for (int i = 0; i < listCubos.Count; i++)
            {
                listCubos[i].GetComponent<MeshRenderer>().enabled = false;
            }
        }
        if (autoRotate)
        {
            transform.Rotate(0, Time.deltaTime * rotationSpeed, 0);
        }
    }
}
