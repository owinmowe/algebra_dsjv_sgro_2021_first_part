﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCubes : MonoBehaviour
{
    [Header("Cubes Related")]
    [SerializeField] int cubesAmmount = 20;
    [SerializeField] int renderLayer = 9;
    [SerializeField] int randomDistance = 50;
    [SerializeField] int distanceFromGround = 2;
    [SerializeField] Material cubeMaterial = null;

    void Awake()
    {
        for (int i = 0; i < cubesAmmount; i++)
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            go.layer = renderLayer;
            Vector3 randPos = Random.insideUnitCircle * randomDistance;
            randPos.z = randPos.y;
            randPos.y = distanceFromGround;
            go.transform.position = randPos;
            go.GetComponent<Renderer>().material = cubeMaterial;
            go.transform.parent = transform;
        }
    }
}
